const usersService = require("../../../services/userService");
const bcrypt = require("bcryptjs");

module.exports = {
  async register(req, res) {
    const { email, userName, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, 10);

    usersService
      .create({
        email: email.toLowerCase(),
        password: hashedPassword,
      })
      .then((createdUser) => {
        res.status(201).json({
          status: "Success",
          message: "Congratulations, you have successfully registered !",
          data: {
            id: createdUser.id,
            email: createdUser.email,
            userName,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
  async getUserData(req, res) {
    const user = await usersService.get(req.params.id);
    if (!user) {
      res.status(404).json({
        status: "FAIL",
        message: `User with id ${req.params.id} not found!`,
      });
      return;
    }

    usersService
      .get(req.params.id)
      .then(() => {
        res.status(200).json({
          status: "success",
          data: user,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
};
