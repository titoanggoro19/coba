const { User } = require("../models");

module.exports = {
  create(inputData) {
    return User.create(inputData);
  },
  findOne(key) {
    return User.findOne(key);
  },
};
