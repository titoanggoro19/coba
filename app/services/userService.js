const usersRepository = require("../repositories/userRepository");

module.exports = {
  create(requestBody) {
    return usersRepository.create(requestBody);
  },
  getOne(key) {
    return usersRepository.findOne(key);
  },
};
